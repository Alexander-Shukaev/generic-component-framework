/****************************************************************************
**
** Copyright (C) VCreate Logic Private Limited, Bangalore
**
** Use of this file is limited according to the terms specified by
** VCreate Logic Private Limited, Bangalore.  Details of those terms
** are listed in licence.txt included as part of the distribution package
** of this file. This file may not be distributed without including the
** licence.txt file.
**
** Contact info@vcreatelogic.com if any conditions of this licensing are
** not clear to you.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Author : Abhishek Patil <abhishek@thezeroth.net>
**
****************************************************************************/

#include <GCF/DeclarativeView>
#include <GCF/ComponentFactory>
#include <GCF/ObjectIterator>
#include <QDeclarativeContext>
#include <QMap>

///////////////////////////////////////////////////////////////////////////////
// DeclarativeView
///////////////////////////////////////////////////////////////////////////////

namespace GCF {
    struct DeclarativeViewData
    {

    };
}

GCF::DeclarativeView::DeclarativeView(QWidget *parent)
    :QDeclarativeView(parent)
{
    d = new DeclarativeViewData;
    GCF::DeclarativeEnvironment::instance().addView(this);
}

GCF::DeclarativeView::~DeclarativeView()
{
    delete d;
}

///////////////////////////////////////////////////////////////////////////////
// DeclarativeEnvironment
///////////////////////////////////////////////////////////////////////////////

namespace GCF {
    struct DeclarativeEnvironmentData
    {
        QList<GCF::DeclarativeView*> declarativeViews;
        QMap<QString, QObject*> nameObjectMap;
    };
}

GCF::DeclarativeEnvironment::DeclarativeEnvironment(QObject* parent)
    :QObject(parent)
{
    d = new DeclarativeEnvironmentData;

    GCF::ComponentFactory* factory = &GCF::ComponentFactory::instance();
    connect(factory, SIGNAL(componentCreated(AbstractComponent*)),
            this, SLOT(slotComponentCreated(AbstractComponent*)));
    connect(factory, SIGNAL(componentDestroyed(AbstractComponent*)),
            this, SLOT(slotComponentDestroyed(AbstractComponent*)));
}

GCF::DeclarativeEnvironment& GCF::DeclarativeEnvironment::instance()
{
    static GCF::DeclarativeEnvironment* theInstance = new DeclarativeEnvironment;
    return *theInstance;
}

GCF::DeclarativeEnvironment::~DeclarativeEnvironment()
{
    delete d;
}

void GCF::DeclarativeEnvironment::clearenv()
{
    d->nameObjectMap.clear();
}

void GCF::DeclarativeEnvironment::addView(DeclarativeView* view)
{
    if(!view)
        return;

    if( d->declarativeViews.contains(view))
        return;

    d->declarativeViews.append(view);
}

void GCF::DeclarativeEnvironment::removeView(DeclarativeView* view)
{
    if(!view)
        return;

    if( !d->declarativeViews.contains(view))
        return;

    d->declarativeViews.removeOne(view);

    // Register all the available qmlNames to new view
    QMap<QString, QObject*>::iterator it;
    for (it = d->nameObjectMap.begin(); it != d->nameObjectMap.end(); ++it)
        view->rootContext()->setContextProperty(it.key(), it.value());
}

void GCF::DeclarativeEnvironment::addQmlName(const QString& qmlName, QObject* obj)
{
    if(d->nameObjectMap.contains(qmlName))
    {
        //qWarning()<< "Can not set two property with the same " << qmlName;
        return;
    }

    d->nameObjectMap.insert(qmlName, obj);

    Q_FOREACH(DeclarativeView* view, d->declarativeViews)
        view->rootContext()->setContextProperty(qmlName, obj);

}

void GCF::DeclarativeEnvironment::removeQmlName(const QString& qmlName)
{
    d->nameObjectMap.remove(qmlName);
}

#include <QDebug>
void GCF::DeclarativeEnvironment::slotComponentCreated(GCF::AbstractComponent* comp)
{
    qDebug() << "Component Created";
    GCF::ComponentGui gui = comp->componentGui();

    for(int i=0; i< gui.nodeCount(); i++)
    {
        GCF::ComponentGuiNode node = gui.node(i);
        if(!node.hasKey("qmlName"))
            continue;

        // Register Qml Name and object
        QString qmlName = node["qmlName"].toString();
        QObject* obj = GCF::ObjectIterator::object(node);
        qDebug()<< "Qml Name " <<  qmlName;
        addQmlName(qmlName, obj);
    }
}

void GCF::DeclarativeEnvironment::slotComponentDestroyed(AbstractComponent* comp)
{
    GCF::ComponentGui gui = comp->componentGui();

    for(int i=0; i< gui.nodeCount(); i++)
    {
        GCF::ComponentGuiNode node = gui.node(i);
        if(!node.hasKey("qmlName"))
            continue;

        // Register Qml Name and object
        QString qmlName = node["qmlName"].toString();
        QObject* obj = GCF::ObjectIterator::object(node);
        addQmlName(qmlName, obj);
    }
}
