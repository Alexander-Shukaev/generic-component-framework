/****************************************************************************
**
** Copyright (C) VCreate Logic Private Limited, Bangalore
**
** Use of this file is limited according to the terms specified by
** VCreate Logic Private Limited, Bangalore.  Details of those terms
** are listed in licence.txt included as part of the distribution package
** of this file. This file may not be distributed without including the
** licence.txt file.
**
** Contact info@vcreatelogic.com if any conditions of this licensing are
** not clear to you.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Author : Abhishek Patil <abhishek@thezeroth.net>
**
****************************************************************************/

#include <GCF/DeclarativeSupport>
#include <GCF/AbstractComponent>
#include <GCF/ObjectIterator>
#include <QApplication>

namespace GCF
{
    struct DeclarativeSupportData
    {

    };
}

GCF::DeclarativeSupport& GCF::DeclarativeSupport::instance()
{
    static GCF::DeclarativeSupport* theInstance = new GCF::DeclarativeSupport(qApp);
    return *theInstance;
}

GCF::DeclarativeSupport::DeclarativeSupport(QObject *parent) :
        QObject(parent)
{
    d = new DeclarativeSupportData;
}


GCF::DeclarativeSupport::~DeclarativeSupport()
{
    delete d;
}

QObject* GCF::DeclarativeSupport::findObject(const QString& iface) const
{
    QByteArray ifaceBA = iface.toLatin1();

    GCF::ObjectIterator objIt;
    QObject* obj = objIt.firstObject();
    while(obj)
    {
        if( obj->inherits(ifaceBA.constData() ))
            return obj;

        obj = objIt.nextObject();
    }

    return 0;
}

QObject* GCF::DeclarativeSupport::getObject(const QString& completeName) const
{
    return ObjectIterator::object(completeName);
}
