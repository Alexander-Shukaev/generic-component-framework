#!/usr/bin/env python
# encoding: utf-8

import subprocess
import sys
import os

project_dir_path = os.path.dirname(os.path.realpath(__file__))

if sys.platform.startswith('win'):
    shell = True
else:
    shell = False

os.chdir(project_dir_path)
subprocess.call(('make'), shell=shell)
