#!/usr/bin/env python
# encoding: utf-8

import subprocess
import sys
import os

project_dir_path = os.path.dirname(os.path.realpath(__file__))

os.chdir(project_dir_path)

# QMake {{{
# -----------------------------------------------------------------------------
config = ('debug_and_release', 'build_all')
config = 'CONFIG += ' + ' '.join(config)

if sys.platform.startswith('win'):
    shell = True
else:
    shell = False

main_pro_file_path = os.path.join(project_dir_path, 'GCF.pro')

subprocess.call(('qmake',
                 '-recursive',
                 config,
                 main_pro_file_path),
                shell=shell)
# -----------------------------------------------------------------------------
# }}} QMake
