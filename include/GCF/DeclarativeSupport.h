/****************************************************************************
**
** Copyright (C) VCreate Logic Private Limited, Bangalore
**
** Use of this file is limited according to the terms specified by
** VCreate Logic Private Limited, Bangalore.  Details of those terms
** are listed in licence.txt included as part of the distribution package
** of this file. This file may not be distributed without including the
** licence.txt file.
**
** Contact info@vcreatelogic.com if any conditions of this licensing are
** not clear to you.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Author : Abhishek Patil <abhishek@thezeroth.net>
**
****************************************************************************/

#ifndef DECLARATIVESUPPORT_H
#define DECLARATIVESUPPORT_H

#include <QObject>

namespace GCF
{
struct DeclarativeSupportData;
class DeclarativeSupport : public QObject
{
    Q_OBJECT

public:
    static DeclarativeSupport& instance();
    ~DeclarativeSupport();

    Q_INVOKABLE QObject* findObject(const QString& iface) const;
    Q_INVOKABLE QObject* getObject(const QString& completeName) const;

protected:
    DeclarativeSupport(QObject *parent = 0);

private:
    DeclarativeSupportData* d;
};
}

#endif // DECLARATIVESUPPORT_H
