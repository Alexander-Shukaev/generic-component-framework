/****************************************************************************
**
** Copyright (C) VCreate Logic Private Limited, Bangalore
**
** Use of this file is limited according to the terms specified by
** VCreate Logic Private Limited, Bangalore.  Details of those terms
** are listed in licence.txt included as part of the distribution package
** of this file. This file may not be distributed without including the
** licence.txt file.
**
** Contact info@vcreatelogic.com if any conditions of this licensing are
** not clear to you.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Author : Abhishek Patil <abhishek@thezeroth.net>
**
****************************************************************************/

#ifndef DECLARATIVEVIEW_H
#define DECLARATIVEVIEW_H

#include <QDeclarativeView>
namespace GCF
{
class AbstractComponent;
class ObjectIterator;

// Class DeclarativeView
struct DeclarativeViewData;
class DeclarativeView : public QDeclarativeView
{
public:
    DeclarativeView(QWidget *parent = 0);
    ~DeclarativeView();

private:
    DeclarativeViewData* d;
};

// Class DeclarativeEnvironment
struct DeclarativeEnvironmentData;
class DeclarativeEnvironment : public QObject
{
    Q_OBJECT
public:
    // Singleton class
    static DeclarativeEnvironment& instance();
    ~DeclarativeEnvironment();

    // Methods
    void clearenv();

    void addView(DeclarativeView* view);
    void removeView(DeclarativeView* view);

    void addQmlName(const QString& qmlName, QObject* obj);
    void removeQmlName(const QString& qmlName);

private slots:
    void slotComponentCreated(AbstractComponent*);
    void slotComponentDestroyed(AbstractComponent*);

protected:
    DeclarativeEnvironment(QObject* parent=0);

private:
    DeclarativeEnvironmentData* d;
};
}
#endif // DECLARATIVEVIEW_H
